import { StyleSheet } from 'react-native';
import tinycolor from "tinycolor2";
import {colors, fontSizes} from '../../config/styles';

export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: tinycolor(colors.darkBlue).setAlpha(.7),
    elevation: 1
  },
  headline: {
    color: "white",
    fontSize: fontSizes.large
  },
  subHeadline: {
    color: "white",
    fontSize: fontSizes.normal
  }
});
