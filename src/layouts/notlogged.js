import React from 'react';
import { View, Text, TextInput } from 'react-native';
import { StackNavigator } from 'react-navigation';
import LoginScreen from '../pages/LoginScreen/LoginScreen';

const NotLoggedUserTab = StackNavigator({
  Home: {
    screen: LoginScreen,
  }
});

export default NotLoggedUserTab;