import { Dimensions, Platform } from 'react-native';
import tinycolor from "tinycolor2";
import RF from "react-native-responsive-fontsize"


export const colors = {
  mainBackground: "white",
  blue: "#4267B2",
  blueGreen: "#00BE82",
  darkBlue: "#333b43",
  lightBlue: '#575d62',
  grey: '#82878C',
  greyBlue: '#272d3a',
  lightGrey: "#E8E9EB",
  brightRed: "#FE0000",
  red: '#BD1F00',
  darkRed: tinycolor("#BD1F00").darken().toString(),
  veryDarkRed: "#660909",
  touchable: 'rgba(0,0,0,.10)',
};

export const buttonShadow = {
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 2
  },
  shadowOpacity: 0.25,
  shadowRadius: 3.84,
  elevation: 5
}

export const sizes = {

};

export const rowIconStyle = {
    resizeMode: 'contain'
}

export const fontSizes = {
  verySmall: RF(1),
  extraSmall: RF(1.25),
  small: RF(1.5),
  normal: RF(1.7),
  large: RF(2.5),
  extraLarge: RF(3),
  veryLarge: RF(3.5),
  extremelyLarge: RF(5), //Use with care
}

export const LIST_STYLES = {
  row: {
    flexDirection: "row",
    paddingHorizontal: 20,
    paddingVertical: 10,
    alignItems: "center",
    justifyContent: "space-between",
    borderBottomWidth: 1,
    borderBottomColor: colors.lightGrey,
    height: "auto",
  },
  rowText: {
    fontSize: fontSizes.normal
  },
  rowSubText: {
    fontSize: fontSizes.small
  }
}

export const HEADER_HEIGHT = 64;