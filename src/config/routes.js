import React, { Component } from 'react';
import {createStackNavigator, createDrawerNavigator} from 'react-navigation';
import { DrawerActions } from 'react-navigation';
import {View, TouchableOpacity, Easing, Animated, Dimensions, ImageBackground } from 'react-native';
import {colors, HEADER_HEIGHT} from './styles'
import applicationHelper from '../libraries/applicationHelper';
import Home from '../pages/Home/Home';
import Light from '../pages/Light/Light';
import Sidebar from '../pages/Sidebar/Sidebar';
import { fadeIn } from 'react-navigation-transitions';


const { width, height } = Dimensions.get('screen');
const HEADER_ICON_MARGIN = 10;

const StackNavigator = createStackNavigator({
    Home: {
        screen: Home
    },
    Light: {
        screen: Light
    }
},{
    headerMode: "none",
    cardStyle: {
        backgroundColor: colors.mainBackground
    },
    transitionConfig: () => fadeIn(),
});

const DrawerNavigator = createDrawerNavigator({
    StackNavigator: {
        screen: StackNavigator
    },
},{
    initialRouteName: 'StackNavigator',
    contentComponent: Sidebar, // Component to show in the sidebar 
    drawerWidth: Math.min(height, width) * 0.8,
});


export default DrawerNavigator;
