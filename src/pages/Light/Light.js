import React, {Component} from 'react';
import {Dimensions, Text, View, Alert, FlatList} from 'react-native';
import styles from './styles';
import WrapRouter from '../../components/WrapRouter';
import { connect } from 'react-redux';
import { ColorWheel } from 'react-native-color-wheel';
import _ from "lodash";
import tinycolor from 'tinycolor2';
import { setColor } from '../../actions';

class Light extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
    };
    var that = this;
}

  render () {
    const {navigation} = this.props;
    var light = navigation.getParam('light', null);
    var color = "#FFFFFF";

    if(light?.attributes.rgb_color) {
      var red = light.attributes.rgb_color[0] || "0"
      var green = light.attributes.rgb_color[1] || "0"
      var blue = light.attributes.rgb_color[2] || "0"

      if(red && green && blue)
        color = tinycolor("rgb("+red+","+green+","+blue+")").toHexString();
    }

    return (
      <WrapRouter>
        {light?.attributes.rgb_color &&
              <ColorWheel
              initialColor={color}
              
              onColorChange={color => {
                var rgbColor = tinycolor(color).toRgb();
                this.props.dispatch(setColor(light.entity_id, [rgbColor.r, rgbColor.g, rgbColor.b]))
              }}
              style={{width: Dimensions.get('window').width}}
              thumbStyle={{ height: 30, width: 30, borderRadius: 30}} />
            }
      </WrapRouter>
    );
  }
}

const mapStateToProps = state => {
  return {
    lights: state.applicationReducer.lights
  }
}
export default connect(
  mapStateToProps
)(Light)

