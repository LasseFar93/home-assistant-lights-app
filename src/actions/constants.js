export const DO_LOGIN='DO_LOGIN'
export const DO_LOGOUT='DO_LOGOUT'
export const LOGIN_SUCCESS='LOGIN_SUCCESS'
export const LOGOUT_SUCCESS='LOGOUT_SUCCESS'
export const LOGIN_ERROR='LOGIN_ERROR'
export const IS_LOADING='IS_LOADING';
export const DONE_LOADING='DONE_LOADING';
export const STORE_CONNECTION_INFO='STORE_CONNECTION_INFO'
export const CONNECT_TO_WEBSOCKET='CONNECT_TO_WEBSOCKET'
export const WEBSOCKET_CONNECTED='WEBSOCKET_CONNECTED'
export const STORE_LIGHTS='STORE_LIGHTS'
export const UPDATE_LIGHT_STATE='UPDATE_LIGHT_STATE'
export const TOGGLE_LIGHT='TOGGLE_LIGHT'
export const INCREMENT_MESSAGE_ID='INCREMENT_MESSAGE_ID'
export const SET_BRIGHTNESS='SET_BRIGHTNESS'
export const SET_COLOR='SET_COLOR'