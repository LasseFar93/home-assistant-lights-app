import { StyleSheet } from 'react-native';
import { colors, fontSizes } from '../../config/styles';
import tinycolor from "tinycolor2";
export const SLIDER_HEIGHT = 35;

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent:  "space-between",
    alignItems: 'stretch',
    height: 100,
    margin: 10,
    borderRadius: SLIDER_HEIGHT/2
  },
  innerContainer: {
    flex: 1,
    padding: 12,
    paddingVertical: fontSizes.normal,
    flexDirection:"row",
    alignItems: "center",
    justifyContent:"space-between"
  },
  titlesContainer: {
    flex: 15,
    flexDirection: 'column',
    alignItems: 'stretch',
    marginBottom: 2,
    height: 60,
    justifyContent: 'center',
    marginLeft: 0
  },
  title: {
    fontSize: fontSizes.large,
    fontWeight: "bold",
    justifyContent: 'center',
    color: "white"
  },
  subtitle: {
    marginTop: 2,
    fontSize: fontSizes.normal,
    color: "white"
  },

  slider: {
    overflow: "hidden",
    alignSelf: "stretch",
    zIndex: 1,
    height: SLIDER_HEIGHT,
    borderRadius: SLIDER_HEIGHT/2,
  },
  sliderShadow: {
    elevation: 8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
  },
  trackStyle: {
    height: SLIDER_HEIGHT,
    borderTopLeftRadius: SLIDER_HEIGHT/2,
    borderBottomLeftRadius: SLIDER_HEIGHT/2,
    paddingRight: SLIDER_HEIGHT/2
  },
  thumbStyle: {
    height: SLIDER_HEIGHT,
    width: SLIDER_HEIGHT,
    borderRadius: SLIDER_HEIGHT/2,
    backgroundColor: "white",
    borderColor: "white",
    borderWidth: 2,
    marginBottom: -30,
    zIndex: 3,
    justifyContent: "center",
    alignItems: "center"
  }
});
