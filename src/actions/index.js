import { DO_LOGIN, DO_LOGOUT, IS_LOADING, DONE_LOADING, CONNECT_TO_WEBSOCKET, TOGGLE_LIGHT, SET_BRIGHTNESS, SET_COLOR } from "./constants";


export const doLogin = (username, password) => {
    return {
        type: DO_LOGIN,
        username, password
    }
}

export const doLogout = () => {
    return {
        type: DO_LOGOUT,
    }
}

export const isLoading = (loading) => {
    return {
        type: IS_LOADING,
        loading
    }
}

export const doneLoading = () => {
    return {
        type: DONE_LOADING,
    }
}

export const connectToWebsoket = (url, token) => {
    return {
        type: CONNECT_TO_WEBSOCKET,
        url, token
    }
}

export const toggleLight = (entity) => {
    return {
        type: TOGGLE_LIGHT,
        entity
    }
}

export const setBrightness = (entity, brightness) => {
    return {
        type: SET_BRIGHTNESS,
        entity, brightness
    }
}

export const setColor = (entity, color) => {
    return {
        type: SET_COLOR,
        entity, color
    }
}