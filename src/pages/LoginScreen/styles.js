import { StyleSheet } from 'react-native';
import {colors, fontSizes} from '../../config/styles';
import tinycolor from "tinycolor2";


export default StyleSheet.create({
    bg: {
        flex: 1,
        padding: 20,
        backgroundColor: colors.darkBlue
    },
    content: { 
        flex: 4,
        alignItems: "stretch",
        justifyContent: 'center',
        marginVertical: 40,
    },
    textInput: {
        color: "white",
        fontSize: 16,
        borderBottomWidth: 1,
        borderBottomColor: "white",
        padding: 5,
        margin: 5,
    },
    button: {
        width: "100%",
        margin: 0,
        marginTop: 10,
        backgroundColor: colors.red,
    },
    error: {
        color: "red",
        fontSize: fontSizes.small,
        padding: 5,
    },
    success: {
        color: "green",
        fontSize: fontSizes.small,
        padding: 5,
    },
    modeChange: {
        padding: 10,
        marginVertical: 10,
        backgroundColor: "rgba(255, 255, 255, .1)",
        borderRadius: 10
    },
    modeChangeText: {
        color: "white",
    },
    image: {
        flex: 1, 
        maxWidth: "70%", 
        alignSelf: "center"
    },
    bottomImage: {
        maxWidth: "60%"
    }
});
