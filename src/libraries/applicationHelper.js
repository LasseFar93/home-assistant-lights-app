import { NavigationActions, DrawerActions } from 'react-navigation';

var module = {};

module.setContainer = function(container) {
    module.container = container;
}


module.navigate = (route, title, params = {}, key) => {
    if(!module.container)
        return;
        
    params.title = title;
    const navigateAction = NavigationActions.navigate({
      routeName: route,
      params,
      key
    });
    module.container.dispatch(navigateAction);
    module.container.dispatch(DrawerActions.closeDrawer())
}

export default module;
