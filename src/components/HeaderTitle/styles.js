import { StyleSheet } from 'react-native';
import { colors, fontSizes, buttonShadow } from '../../config/styles';

export default StyleSheet.create({
  title: {
    fontWeight: 'bold',
    color: "#fff",
    fontSize: fontSizes.large,
  },
  subtitle: {
    color: "#fff",
    fontSize: fontSizes.small,
    marginTop: -5
  }
});
