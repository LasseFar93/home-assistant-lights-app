import BackgroundTimer from 'react-native-background-timer';

setTimeout = (fn, ms = 0) => BackgroundTimer.setTimeout(fn, ms) 
setInterval = (fn, ms = 0) => BackgroundTimer.setInterval(fn, ms) 
clearTimeout = (fn, ms = 0) => BackgroundTimer.clearTimeout(fn, ms) 
clearInterval = (fn, ms = 0) => BackgroundTimer.clearInterval(fn, ms) 

export default {};
