import React from 'react';
import { Text, View } from 'react-native';
import styles from './styles';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import PropTypes from 'prop-types';
import Touchable from 'react-native-platform-touchable';

const Button = (props) => {
  const { text, subtext, icon, onPress, style, textStyle } = props;
  return (
    <Touchable style={[styles.button, style]} onPress={onPress}>
      <View>
        {text != "" && text != undefined &&
        <Text style={[styles.buttonText, textStyle]}>
          {text}
        </Text>
        }
        {subtext != "" && subtext != undefined &&
        <Text style={[styles.buttonText, styles.buttonSubText, textStyle]}>
          {subtext}
        </Text>
        }
        {icon != "" && icon != undefined &&
          <Icon style={{marginTop: 5}} name={icon} size={30} color="white" />
          }
      </View>
    </Touchable>
  );
};

Button.propTypes = {
  text: PropTypes.string,
  onPress: PropTypes.func,
};

export default Button;
