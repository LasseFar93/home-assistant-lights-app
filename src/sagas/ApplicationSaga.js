import { call, put, takeEvery, takeLatest, select, throttle } from 'redux-saga/effects'
import { WEBSOCKET_CONNECT, WEBSOCKET_OPEN, WEBSOCKET_SEND, WEBSOCKET_MESSAGE } from '@giantmachines/redux-websocket';
import { DONE_LOADING, STORE_CONNECTION_INFO, CONNECT_TO_WEBSOCKET, WEBSOCKET_CONNECTED, STORE_LIGHTS, UPDATE_LIGHT_STATE, TOGGLE_LIGHT, SET_BRIGHTNESS, INCREMENT_MESSAGE_ID, SET_COLOR} from "../actions/constants";

import _ from "lodash";
const getConnectionInfo = state => state.applicationReducer.connectionInfo;
const getLastMessageId = state => state.applicationReducer.messageId || 1

function* handleMessage(action) {
  var payloadData = action.payload?.data ? JSON.parse(action.payload.data) : null;
  if(!payloadData)
    return;

  if(payloadData.type == "auth_required") {
    const connectionInfo = yield select(getConnectionInfo);
    yield put({ type: WEBSOCKET_SEND, payload: {"type": "auth", "access_token": connectionInfo.token} });
  }
  else if(payloadData.type == "auth_ok") {
    yield put({type: WEBSOCKET_CONNECTED})
    yield put({type: DONE_LOADING})
    var messageId = yield select(getLastMessageId);
    yield put({ type: WEBSOCKET_SEND, payload: {"id": messageId, "type": "get_states"} });
    yield put({ type: INCREMENT_MESSAGE_ID })
    messageId = yield select(getLastMessageId);
    yield put({ type: WEBSOCKET_SEND, payload: {"id": messageId, "type": "subscribe_events", "event_type": "state_changed"} });
    yield put({ type: INCREMENT_MESSAGE_ID })

  }
  else if(payloadData.type == "event") {
    var data = payloadData.event.data
    var splits = data.entity_id.split('.')
    if(splits && splits.length && splits[0] == "light") {
      var light = data.new_state;
    if(light)
      yield put({type: UPDATE_LIGHT_STATE, light })
    }
  }
  else if(payloadData.type == "result" && payloadData.id == 1) {
    var lights = _.filter(payloadData.result, (result) => {
      var splits = result.entity_id.split('.')
      return splits && splits.length && splits[0] == "light"
    });
    if(lights) {
      yield put({ type: STORE_LIGHTS, lights})
    }
  }
}

function* handleConnectToWebsocket(action) {
  yield put({
    type: WEBSOCKET_CONNECT,
    payload: {
      url: action.url,
    }
  })
  yield put({
    type: STORE_CONNECTION_INFO,
    url: action.url, token: action.token
  });
}

function* handleToggleLight(action) {
  var messageId = yield select(getLastMessageId);

  yield put({ type: WEBSOCKET_SEND, payload: {
    "id": messageId,
    "type": "call_service",
    "domain": "light",
    "service": "toggle",
    // Optional
    "service_data": {
      "entity_id": action.entity
    }} 
  });
  yield put({ type: INCREMENT_MESSAGE_ID })
}

function* handleSetBrightness(action) {
  var messageId = yield select(getLastMessageId);

  yield put({ type: WEBSOCKET_SEND, payload: {
    "id": messageId,
    "type": "call_service",
    "domain": "light",
    "service": "turn_on",
    "service_data": {
      "entity_id": action.entity,
      "brightness": action.brightness
    }} 
  });
  yield put({ type: INCREMENT_MESSAGE_ID })
}

function* handleSetColor(action) {
  var messageId = yield select(getLastMessageId);

  yield put({ type: WEBSOCKET_SEND, payload: {
    "id": messageId,
    "type": "call_service",
    "domain": "light",
    "service": "turn_on",
    "service_data": {
      "entity_id": action.entity,
      "rgb_color": action.color
    }} 
  });
  yield put({ type: INCREMENT_MESSAGE_ID })
}

function* watchWebsocketMessageReceived() {
  yield takeEvery(WEBSOCKET_MESSAGE, handleMessage);
}

function* watchConnectWebsocket() {
  yield takeEvery(CONNECT_TO_WEBSOCKET, handleConnectToWebsocket);
}

function* watchToggleLight() {
  yield takeEvery(TOGGLE_LIGHT, handleToggleLight)
}

function* watchSetBrightness() {
  yield throttle(1000, SET_BRIGHTNESS, handleSetBrightness)
}

function* watchSetColor() {
  yield throttle(1000, SET_COLOR, handleSetColor)
}

export default watchWebsocketMessageReceived;

export {
  watchConnectWebsocket,
  watchToggleLight,
  watchSetBrightness,
  watchSetColor
}