
import { StyleSheet, Dimensions } from 'react-native';
import { colors, fontSizes, HEADER_HEIGHT } from '../../config/styles';
import tinycolor from "tinycolor2";


export default StyleSheet.create({
    container: {
        flex: 1, 
        alignItems: "center", 
        justifyContent: "space-evenly",
    },
    buttonImage: {
        flex: 1,
        marginBottom: 20
    },
    buttonHeadLine: {
        color: colors.darkBlue, 
        fontSize: fontSizes.large
    },
    buttonSubHeadLine: {
        color: colors.lightBlue,
        fontSize: fontSizes.normal
    },
    modal: {
        marginTop: -HEADER_HEIGHT
      }
});