import React from 'react';
import { View, ActivityIndicator, Text } from 'react-native';
import styles from './styles';
import PropTypes from 'prop-types';
import {
  WaveIndicator
} from 'react-native-indicators';

const Loading = (props) => {
  return (
    <View style={[styles.container, props.style]}>
      <WaveIndicator  
        animating
        size={80}
        color="white"
        waveFactor={0.60}
        style={{ flex: 0 }}
      />
      <Text style={styles.headline}>{props.headerText}</Text>
      <Text style={styles.subHeadline}>{props.subHeaderText}</Text>
    </View>
  );
};

export default Loading;
