import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';
import {Dimensions, Text, View, Alert, FlatList} from 'react-native';
import { DrawerActions } from 'react-navigation';
import LightRow from '../../components/LightRow';
import {colors, fontSizes} from '../../config/styles';
import styles from './styles';
import WrapRouter from '../../components/WrapRouter';
import { connect } from 'react-redux';
import { ColorWheel } from 'react-native-color-wheel';
import Modal from 'react-native-modal'
import _ from "lodash";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
    };
    this.navigateToScreen=this.navigateToScreen.bind(this);
    var that = this;
}
  navigateToScreen = (route, title, params = {}, key) => {
    params.title = title;
    const navigateAction = NavigationActions.navigate({
      routeName: route,
      params,
      key
    });
    this.props.navigation.dispatch(navigateAction);
    this.props.navigation.dispatch(DrawerActions.closeDrawer())
  }

  onLayout = (e) => {
    this.setState({
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
    })
  }

  onLongPress(light) {
    const navigateAction = NavigationActions.navigate({
      routeName: "Light",
      params: {light},
      key: "Light",
    });
    this.props.navigation.dispatch(navigateAction);
  }


  render () {
    return (
      <WrapRouter>
        <FlatList
          style={{flex: 1}}
          scrollEnabled={true}
          data={this.props.lights}
          renderItem={({item}) => {
            if(!item.attributes?.hidden) 
              return <LightRow key={item.entity_id} light={item} onLongPress={() => this.onLongPress(item)}/>
          }}
       />
      </WrapRouter>
    );
  }
}

const mapStateToProps = state => {
  return {
    lights: state.applicationReducer.lights
  }
}
export default connect(
  mapStateToProps
)(Home)

