import './libraries/ReactotronConfig';

import {Alert, Platform, Text, View, TextInput, AsyncStorage, ScrollView, Dimensions} from 'react-native';
import { colors, fontSizes } from './config/styles';
import DrawerNavigator from './config/routes';
import Loading from './components/Loading';
import LoginScreen from './pages/LoginScreen/LoginScreen';
import React from 'react';
import { connect } from 'react-redux'

import applicationHelper from './libraries/applicationHelper';


class AppHome extends React.Component {
  constructor(props) {
    super(props);
  }


  render() {
    const {connectionInfo, loading } = this.props;

    var screenToShow = <LoginScreen style={{flex: 1}}/>;
    var error = this.props.loginError || this.props.locationError || this.props.applicationError || null;

    if(connectionInfo?.status == 1)
      screenToShow = <DrawerNavigator ref={navigatorRef => {applicationHelper.setContainer(navigatorRef)}} style={{flex: 1}}  {...this.props}/>

    return (
      <View style={{flex: 1}}> 

        {screenToShow}

       
          {(loading) &&
            <Loading headerText={loading.headerText} subHeaderText={loading.subHeaderText}/>
          }

      
      </View>
    )
  }

}

const mapStateToProps = state => {
  return {
   
    loading: state.applicationReducer.loading,
    connectionInfo: state.applicationReducer.connectionInfo
  }
}

export default connect(
  mapStateToProps
)(AppHome)