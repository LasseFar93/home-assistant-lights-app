import update from 'immutability-helper';
import {AsyncStorage, Alert} from 'react-native';
import _ from 'underscore';

var module = {};

module.getValues = function () {
    var keys = arguments;
    return AsyncStorage.multiGet(keys, (err, values) => {
        return _.map(keys, (key, i) => values[i] && values[i].length > 1 ? values[i][1] : null);
    });
};

export default module;
