import React from 'react';
import { Keyboard, View, ScrollView, TextInput, Text, Image, Linking, Platform  } from 'react-native';
import { connect } from 'react-redux';
import { connectToWebsoket, doneLoading, isLoading } from '../../actions';
import {AsyncStorage} from 'react-native';
import Button from '../../components/Button';
import styles from './styles';
import { WEBSOCKET_CONNECT, WEBSOCKET_OPEN } from '@giantmachines/redux-websocket'
import loginInfo from './login_info'  

class LoginScreen extends React.Component {
    constructor(props) {
        super(props);
        this.logon = this.logon.bind(this);
        this.connect = this.connect.bind(this);
        this.state = {
            url: loginInfo.url,
            token: loginInfo.token
        }
        that = this;
    }

    async connect() {
        this.props.dispatch(isLoading());

        this.props.dispatch(
            connectToWebsoket(this.state.url, this.state.token)
        )
    }

    logon() {
        var url = this.state.url;
        var token = this.state.token;

        if (url && token) {
            this.props.dispatch(doneLoading());
            this.props.dispatch(doLogin(url, token))
        }
        this.props.dispatch(doneLoading());
    }

    render() {
        return <ScrollView style={styles.bg} contentContainerStyle={{flexGrow: 1, justifyContent: "center"}} keyboardShouldPersistTaps="always" >

            <View style={styles.content}>
                <TextInput 
                    placeholder="URL"
                    value={this.state.url} 
                    underlineColorAndroid='transparent' 
                    placeholderTextColor="white" 
                    style={styles.textInput} 
                    onChangeText={(url) => this.setState({ url })}
                />
                <TextInput 
                    placeholder="Token"
                    value={this.state.token} 
                    underlineColorAndroid='transparent' 
                    placeholderTextColor="white" 
                    style={styles.textInput} 
                    onChangeText={(token) => this.setState({ token })}
                />

                <Button style={styles.button} textStyle={{fontSize: 16}}  text='Connect to Home Assistant' onPress={this.connect}/>
            </View>
        </ScrollView>
    }

}


const mapStateToProps = state => {
    return {
    }
}


export default connect(
    mapStateToProps
)(LoginScreen)
