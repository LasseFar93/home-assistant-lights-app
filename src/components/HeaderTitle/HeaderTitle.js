import React from 'react';
import { Text, View } from 'react-native';
import styles from './styles';
import { connect } from 'react-redux';

class HeaderTitle extends React.Component {
  render() {
    const {title, subtitle} = this.props;
    var subtitleText = subtitle;

    return (
      <View>
      <Text style={[styles.title]}>{title ||"MyLights"}</Text>

      {subtitleText &&
        <Text style={[styles.subtitle]}>{subtitleText}</Text>
      }
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
  }
}
export default connect(
  mapStateToProps
)(HeaderTitle)