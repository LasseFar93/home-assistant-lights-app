import { StyleSheet, Dimensions, Platform } from 'react-native';
import { colors, fontSizes } from '../../config/styles';
import tinycolor from "tinycolor2";

const TEXT_COLOR = "#d1cece";

export default StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 20,
        backgroundColor: colors.darkBlue,
        paddingVertical: Platform.OS == "ios" ? 20 : 0
    },
    profileRecap: {
        borderColor: colors.greyBlue,
        borderBottomWidth: 1
    },
    row: {
        paddingHorizontal: 0,
        paddingVertical: 0,
        height: 60,
        borderColor: colors.greyBlue
    },
    recapText: {
        color: TEXT_COLOR
    },  
    rowText: {
        color: TEXT_COLOR,
    },
    rowSubText: {
        color: colors.grey,
    }
});
