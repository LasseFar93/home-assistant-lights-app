import { combineReducers } from "redux";
import applicationReducer from "./ApplicationReducer";

// Combine reducers
const rootReducer = combineReducers({
  applicationReducer
});

export default rootReducer;
