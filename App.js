import React from 'react';
import Application from './src/application'
import { Provider } from "react-redux";
import websocket from '@giantmachines/redux-websocket'
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import itemApp from './src/reducers'
import rootSaga from './src/sagas'
import Reactotron from 'reactotron-react-native';

const sagaMiddleware = createSagaMiddleware()

const store = __DEV__ ? Reactotron.createStore(itemApp,
  applyMiddleware(sagaMiddleware, websocket)) : createStore(itemApp,
    applyMiddleware(sagaMiddleware, websocket));

sagaMiddleware.run(rootSaga);

export default class App extends React.Component {


  render() {
    return (
      <Provider store={store}>
        <Application />
      </Provider>
    );
  }
}