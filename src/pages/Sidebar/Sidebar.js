import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {ScrollView, Text, View, Alert} from 'react-native';
import { NavigationActions, DrawerActions, StackActions } from 'react-navigation';
import styles from './styles';
import { colors, fontSizes } from '../../config/styles';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import Touchable from 'react-native-platform-touchable';
import { doLogout } from '../../actions';

class Home extends Component {
  constructor(props) {
    super(props);

    this.navigateToScreen=this.navigateToScreen.bind(this);
    this.logout=this.logout.bind(this);
  }
  navigateToScreen = (route, params, key) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route,
      params,
      key: key || route
    });
    this.props.navigation.dispatch(navigateAction);
    this.props.navigation.dispatch(DrawerActions.closeDrawer())
  }
  
  goHome = () => {
    const navigateAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "Home" })],
    });
    this.props.navigation.dispatch(navigateAction)
    this.props.navigation.dispatch(DrawerActions.closeDrawer())
  }

  logout(){
    Alert.alert("Ønsker du at logge ud?", "Bekræft venligst at du vil logge ud", [
      {text: "Forbliv logget ind", onPress: () => {}},
      {text: "Log ud", onPress: () => this.props.dispatch(doLogout())}
    ])
  }

  render () {
    var ROWS = [
    ]
    
    return (
      <View style={{flex: 1}}>
        <View style={styles.container}>
          <ScrollView>
            <View>

            </View>
          </ScrollView>
        </View>
        <Touchable onPress={this.logout}>
          <View style={{flexDirection: "row", justifyContent: "flex-start", alignItems: "center", backgroundColor: colors.darkRed, paddingHorizontal: 20, paddingVertical: 10, marginBottom: -1}}>
            <Icon name="sign-out" size={fontSizes.normal} color="white" style={{marginRight: 5}}/>
            <Text style={{fontSize: fontSizes.normal, color: "white"}}>Logout</Text>
          </View>
        </Touchable>
      </View>
    );
  }
}

Home.propTypes = {
  navigation: PropTypes.object
};


const mapStateToProps = state => {
  return {
  }
}


export default connect(
  mapStateToProps
)(Home)