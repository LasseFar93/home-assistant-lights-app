import { StyleSheet } from 'react-native';
import { colors, fontSizes, buttonShadow } from '../../config/styles';

export default StyleSheet.create({
  button: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: colors.darkBlue,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5,
    flex: 0, 
    borderRadius: 10,
    ...buttonShadow

  },
  buttonText: {
    color: "white",
    fontSize: fontSizes.small,
    fontWeight: '500',
  },
  buttonSubText: {
    color: "white",
    fontSize: fontSizes.small + 3,
    fontWeight: '500',
  },
});
