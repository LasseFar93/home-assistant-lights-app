import { all } from 'redux-saga/effects';
import watchWebsocketMessageReceived, {watchConnectWebsocket, watchToggleLight, watchSetBrightness, watchSetColor} from './ApplicationSaga';

export default function* rootSaga() {
    yield all([
        watchWebsocketMessageReceived(),
        watchConnectWebsocket(),
        watchToggleLight(),
        watchSetBrightness(),
        watchSetColor()
    ])
  }