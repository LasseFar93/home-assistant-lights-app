import { NativeModules } from "react-native";
import Reactotron, { asyncStorage, trackGlobalErrors, overlay } from 'reactotron-react-native';
import { reactotronRedux } from 'reactotron-redux';
if (__DEV__) {
  var host = NativeModules.SourceCode.scriptURL;
  if (host) {
    var index = host.indexOf("://");
    if (index > -1)
      host = host.substring(index + 3);

    index = host.lastIndexOf(":");
    if (index > -1)
      host = host.substring(0, index);
  }

  Reactotron
    .configure({
      host: host,
      name: 'My Lights'    
    }) // controls connection & communication settings
    .useReactNative() // add all built-in react native plugins
    .use(reactotronRedux()) //  <- here i am!  
    .use(asyncStorage()) // <--- here we go!
    .use(overlay()) // <--- here we go!  
    .use(trackGlobalErrors()) // <--- here we go!
    .connect(); // let's connect!
    console.tron = Reactotron;
}
else {
  console.tron = console;
}



