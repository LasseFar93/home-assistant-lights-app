import React, {Component} from 'react';
import { Animated, View, Text, StyleSheet, Image, TouchableHighlight, TouchableWithoutFeedback, Alert, Switch } from 'react-native';
import styles, {SLIDER_HEIGHT} from './styles';
import {colors} from '../../config/styles';
import tinycolor from 'tinycolor2';
import { toggleLight, setBrightness } from '../../actions';
import { connect } from 'react-redux';
import Slider from 'react-native-slider';
import Modal from 'react-native-modal';

class LightRow extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isSliding: false,
      brightness: props.light.attributes.brightness
    }
    this.position = new Animated.ValueXY(0, 0);
    this.color = new Animated.Value(0);
    this.colorAnimation = Animated.timing(this.color, { duration: 1000, toValue: 1 });
    this.backgroundColorFromLight = "grey";
    this.oldBackgroundColorFromLight = "grey";
  }

  componentWillMount() {
    const {light} = this.props;

    this.backgroundColorFromLight = this.getBackgroundColor(light);;
    this.animateBackgroundColor();
  }

  componentWillUpdate(nextProps, nextState) {
    const {light} = nextProps;
    this.oldBackgroundColorFromLight = this.backgroundColorFromLight;
    this.backgroundColorFromLight = this.getBackgroundColor(light);
    this.animateBackgroundColor();
    if(this.state.brightness != light.attributes.brightness)
      this.setState({brightness: light.attributes.brightness})
  } 

  animateBackgroundColor() {
    this.color.setValue(0);
    this.colorAnimation.start();
  }

  getBackgroundColor(light) {
    var backgroundColor = "grey";

    if(light.state == "on")
      if(light.attributes.rgb_color) {
        var red = light.attributes.rgb_color[0] || "0"
        var green = light.attributes.rgb_color[1] || "0"
        var blue = light.attributes.rgb_color[2] || "0"
        if(red && green && blue)
          backgroundColor = "rgb("+red+","+green+","+blue+")";
      }
      else {
        backgroundColor = "rgb(253, 216, 53)";
      }
      
      if(this.state.brightness !== null) {
        var percentBrightness = 100 - (this.state.brightness*100 / 255);
        var darkenAmount = percentBrightness/2;
        backgroundColor = tinycolor(backgroundColor).brighten(-darkenAmount).toRgbString();
      }

      return backgroundColor;
  }

  render(){
    const {light} = this.props;


    var backgroundColor = this.color.interpolate({
        inputRange: [0, 1],
        outputRange: [this.oldBackgroundColorFromLight, this.backgroundColorFromLight]
      });

    var title = light.attributes?.friendly_name || light.entity_id;

    return (
        <TouchableHighlight underlayColor = {colors.touchable} onLongPress={() => {this.props.onLongPress ? this.props.onLongPress(this.props) : () => {}}} onPress={() => {this.props.onPress ? this.props.onPress(this.props) : this.props.dispatch(toggleLight(light.entity_id))}}>
          <Animated.View style={[styles.container, {backgroundColor}, this.props.rowStyle]}>
            <View style={styles.innerContainer}>
              <View style={[styles.titlesContainer, this.props.titleContainerStyle]}>
                <Text ellipsizeMode = "tail" style={[styles.title, this.props.titleStyle]}>
                  {title}
                </Text>
                {
                  light.state == "unavailable" &&
                  <Text
                    numberOfLines = {2}
                    ellipsizeMode = "tail"
                    style={[styles.subtitle, this.props.subtitleStyle]}>
                    Unavailable
                  </Text>
                }
              </View>
            </View>
            <Slider 
              value={this.state.brightness}
              step={5} 
              animationType="spring" 

              style={[styles.slider]}
              trackStyle={[styles.trackStyle]}
              thumbStyle={{height: SLIDER_HEIGHT, width: SLIDER_HEIGHT, borderRadius: SLIDER_HEIGHT/2}} 
              thumbTintColor={tinycolor(backgroundColor).darken().toRgbString()}
              minimumTrackTintColor={"rgba(255, 255, 255, 0.5)"} 
              maximumTrackTintColor={"rgba(255, 255, 255, 0.3)"}
              minimumValue={1}
              maximumValue={255}
              onSlidingComplete={(brightness) => this.setState({brightness}, () => this.props.dispatch(setBrightness(light.entity_id, brightness)))}
            />
            
          </Animated.View>
        </TouchableHighlight>
    )
  }
}

const mapStateToProps = state => {
  return {
  }
}
export default connect(
  mapStateToProps
)(LightRow)

