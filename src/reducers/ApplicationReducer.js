import {AsyncStorage} from 'react-native';
import _ from "lodash";
import update from 'immutability-helper';
import produce from "immer"

import { STORE_CONNECTION_INFO, IS_LOADING, DONE_LOADING, WEBSOCKET_CONNECTED, STORE_LIGHTS, UPDATE_LIGHT_STATE, INCREMENT_MESSAGE_ID } from "../actions/constants";

const ApplicationReducer = (state = {}, action) => {
  switch (action.type) {
    case IS_LOADING: {
      action.loading = action.loading || {};
      return {...state, loading: {headerText: action.loading.headerText || "Vent Venligst", subHeaderText: action.loading.subHeaderText || ""}}
    }
    case DONE_LOADING: {
      return { ...cleanUp(state), loading: null }
    }
    case STORE_CONNECTION_INFO: {
      return { ...cleanUp(state), connectionInfo: {
        status: 0,
        url: action.url,
        token: action.token
      }}
    }
    case WEBSOCKET_CONNECTED: {
      return { ...state, 
        connectionInfo: {...state.connectionInfo, status: 1}
      }
    }
    case INCREMENT_MESSAGE_ID: {
      return { ...state,
        messageId: state.messageId ? state.messageId += 1 : 2
      }
    }
    case STORE_LIGHTS: {
      return {...state, lights: action.lights}
    }
    case UPDATE_LIGHT_STATE: {
      return produce(state, newState => {
        var index = _.findIndex(newState.lights, light => {
          return light.entity_id == action.light.entity_id;
        });

        if(index > -1)
          newState.lights[index] = action.light;

          return newState
      })
    }
    default:
      return cleanUp(state);
  }
};


// For passing error property only on error, next time reducer will clear off error
//can be used for clearing flags also
const cleanUp = (state) => {
  return { ...state, error: null }
}

export default ApplicationReducer;
