import React from 'react';
import { connect } from 'react-redux'
import { SafeAreaView } from 'react-navigation';

class WrapRouter extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <SafeAreaView style={[{flex: 1, backgroundColor: "black"}, this.props.safeAreaViewStyle]}>
                {this.props.children}
            </SafeAreaView>
        )
    }
}

const mapStateToProps = state => {
    return {
    }
  }
  
  export default connect(
    mapStateToProps
  )(WrapRouter)

